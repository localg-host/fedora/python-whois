%global pkgname whois
%global srcname python-whois

Name:          python-%{pkgname}
Summary:       Whois querying and parsing of domain registration information
License:       MIT
URL:           https://github.com/richardpenman/pywhois

Version:       0.7.2
Release:       1%{?dist}
Source0:       %{pypi_source}

BuildArch:     noarch


%global _description %{expand:
Whois querying and parsing of domain registration information.}

%description %_description


%package -n python3-%{pkgname}
Summary:       %{summary}

BuildRequires: python3-devel
BuildRequires: %{py3_dist future}
BuildRequires: %{py3_dist nose}
BuildRequires: %{py3_dist python-dateutil}
BuildRequires: %{py3_dist simplejson}

# This is not automatically picked up by the dependency generator
Requires:      %{py3_dist python-dateutil}

%{?python_provide:%python_provide python3-%{pkgname}}


%description -n python3-%{pkgname} %_description


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%py3_build


%install
%py3_install


%check
# Tests require network access
#%%{__python3} setup.py test


%files -n python3-%{pkgname}
%doc README.rst
# https://github.com/richardpenman/pywhois/pull/20
#%%license LICENSE.txt
%{python3_sitelib}/%{pkgname}
%{python3_sitelib}/python_whois-%{version}-py*.egg-info


%changelog
* Fri Nov 01 2019 Mathieu Bridon <bochecha@daitauha.fr> - 0.7.2-1
- Initial package for Fedora.
